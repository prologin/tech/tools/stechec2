// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright (c) 2022 Association Prologin <association@prologin.org>
// Copyright (c) 2022 Léo Lanteri Thauvin
{% import 'macros/cxx.jinja2' as cxx %}

#include <js/Array.h>
#include <js/CompilationAndEvaluation.h>
#include <js/Conversions.h>
#include <js/ErrorReport.h>
#include <js/Initialization.h>
#include <jsapi.h>
#include <mozilla/mozalloc_abort.h>

#include <iostream>
#include <string>
#include <vector>

{% for enum in game.enum %}
{{ cxx.decl_enum(enum) }}

{% endfor %}
{% for struct in game.struct %}
{{ cxx.decl_struct(struct) }}

{% endfor %}
extern "C" {
{% for func in game.function %}
{{ func.fct_summary|cxx_comment(doc=True) }}
{{ func|cxx_prototype(prefix='api_') }};

{% endfor %}
}

/// Utility for converting a `JSString` into a C++ `std::string`
bool js_encode_string(JSContext* cx, JSString* in, std::string& out) {
    JS::AutoCheckCannotGC _nogc;
    JSLinearString* s = JS_EnsureLinearString(cx, in);
    if (!s)
        return false;
    size_t len = JS::GetDeflatedUTF8StringLength(s);
    out.resize(len);
    JS::DeflateStringToUTF8Buffer(s, out);
    return true;
}

template <typename CxxType>
void cxx_to_js(JSContext* cx, CxxType in, JS::MutableHandleValue out) {
    return in.__if_that_triggers_an_error_there_is_a_problem;
}

template <typename CxxType>
bool js_to_cxx(JSContext* cx, JS::HandleValue in, CxxType& out) {
    return CxxType::__if_that_triggers_an_error_there_is_a_problem;
}

template<>
void cxx_to_js(JSContext* cx, int i, JS::MutableHandleValue out) {
    out.setInt32(i);
}

template <>
bool js_to_cxx(JSContext* cx, JS::HandleValue in, int& out) {
    if (!in.isInt32())
        return false;
    out = in.toInt32();
    return true;
}

template <>
void cxx_to_js(JSContext* cx, double x, JS::MutableHandleValue out) {
    out.setNumber(x);
}

template <>
bool js_to_cxx(JSContext* cx, JS::HandleValue in, double& out) {
    if (!in.isNumber())
        return false;
    out = in.toNumber();
    return true;
}

template <>
void cxx_to_js(JSContext* cx, std::string s, JS::MutableHandleValue out) {
    JSString* str = JS_NewStringCopyN(cx, s.data(), s.size());
    if (!str) {
        JS_ReportOutOfMemory(cx);
        return;
    }
    out.setString(str);
}

template <>
bool js_to_cxx(JSContext* cx, JS::HandleValue in, std::string& out) {
    if (!in.isString())
        return false;
    JSString* str = in.toString();
    return js_encode_string(cx, str, out);
}

template <>
void cxx_to_js(JSContext* cx, bool b, JS::MutableHandleValue out) {
    out.setBoolean(b);
}

template <>
bool js_to_cxx(JSContext* cx, JS::HandleValue in, bool& out) {
    if (!in.isBoolean())
        return false;
    out = in.toBoolean();
    return true;
}

template <typename CxxType>
void cxx_to_js_array(JSContext* cx, const std::vector<CxxType>& in, 
                     JS::MutableHandleValue out) {
    JS::RootedValueVector v(cx);
    if (!v.resize(in.size())) {
        JS_ReportOutOfMemory(cx);
        return;
    }
    for (size_t i = 0; i < in.size(); i++)
        cxx_to_js<CxxType>(cx, in[i], v[i]);
    out.setObject(*JS::NewArrayObject(cx, v));
}

template <typename CxxType>
bool js_to_cxx_array(JSContext* cx, JS::HandleValue in, 
                     std::vector<CxxType>& out) {
    bool is_array;
    if (!JS::IsArrayObject(cx, in, &is_array) || !is_array)
        return false;
    JS::RootedObject arr(cx, &in.toObject());

    uint32_t len;
    if (!JS::GetArrayLength(cx, arr, &len))
        return false;

    out.clear();
    out.reserve(len);
    for (uint32_t i = 0; i < len; i++) {
        JS::RootedValue jsval(cx);
        CxxType cxxval;
        if (!JS_GetElement(cx, arr, i, &jsval) ||
            !js_to_cxx<CxxType>(cx, jsval, (CxxType&)cxxval)) {
            return false;
        }
        out.push_back(cxxval);
    }
    return true;
}

{% for enum in game.enum %}
template<>
void cxx_to_js(JSContext* cx, {{ enum.enum_name }} in, JS::MutableHandleValue out) {
    cxx_to_js(cx, (int)in, out);
}

template<>
bool js_to_cxx(JSContext* cx, JS::HandleValue in, {{ enum.enum_name }}& out) {
    return js_to_cxx(cx, in, (int&)out);
}

{% endfor %}

{% for struct in game.struct %}
template<>
void cxx_to_js(JSContext* cx, {{ struct.str_name }} in,
               JS::MutableHandleValue out) {
    JS::RootedValueVector v(cx);
    if (!v.resize({{ struct.str_field|length }})) {
        JS_ReportOutOfMemory(cx);
        return;
    }
    {% for field_name, field_type, _ in struct.str_field %}
    {{ field_type|cxx_to_js }}(cx, in.{{ field_name }}, v[{{ loop.index0 }}]);
    {% endfor %}

    {% if struct.str_tuple %}
    out.setObject(*JS::NewArrayObject(cx, v));
    {% else %}
    JS::RootedObject obj(cx, JS_NewPlainObject(cx));
    {% for field_name, _, _ in struct.str_field %}
    JS_SetProperty(cx, obj, "{{ field_name|camel_case(upper=False) }}",
                   v[{{ loop.index0 }}]);
    {% endfor %}
    out.setObject(*obj);
    {% endif %}
}

template<>
bool js_to_cxx(JSContext* cx, JS::HandleValue in, {{ struct.str_name }}& out) {
    if (!in.isObject())
        return false;
    JS::RootedObject obj(cx, &in.toObject());
    {% if struct.str_tuple %}
    bool is_array;
    uint32_t len;
    if (!JS::IsArray(cx, obj, &is_array) || !is_array ||
        !JS::GetArrayLength(cx, obj, &len) ||
        len != {{ struct.str_field|length }}) {
        return false;
    }
    {% endif %}
    
    {% for field_name, _, _ in struct.str_field %}
    JS::RootedValue {{ field_name }}(cx);
    {% endfor %}

    return true
        {% for field_name, field_type, _ in struct.str_field %}
        {% if struct.str_tuple %}
        && JS_GetElement(cx, obj, {{ loop.index0 }}, &{{ field_name }})
        {% else %}
        && JS_GetProperty(cx, obj, "{{ field_name|camel_case(upper=False) }}", &{{ field_name }})
        {% endif %}
        && {{ field_type|js_to_cxx }}(cx, {{ field_name }}, out.{{ field_name }})
        {% endfor %}
    ;
}
{% endfor %}

{% for func in game.function %}
bool js_{{ func.fct_name }}(JSContext* cx, unsigned argc, JS::Value* vp) {
    if (argc != {{ func.fct_arg|length }}) {
        JS_ReportErrorASCII(cx, "`{{ func.fct_name|camel_case(upper=False) }}` "
                            "expected {{ func.fct_arg|length }} arguments, "
                            "got %d", argc);
        return false;
    }
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);

    {% for arg_name, arg_type, _ in func.fct_arg %}
    {{ arg_type|cxx_type }} {{ arg_name }};
    if (!{{ arg_type|js_to_cxx }}(cx, args[{{ loop.index0 }}], {{ arg_name }})) {
        JS_ReportErrorASCII(cx, "`{{ func.fct_name|camel_case(upper=False) }}` "
                            "got bad {{ loop.index0 }}th argument, "
                            "expected type `{{ arg_type|ts_type }}`");
        return false;
    }
    {% endfor %}

    {% if func is returning %}
    {{ func.fct_ret_type|cxx_to_js }}(
        cx,
        api_{{ func.fct_name }}({{ func.fct_arg|cxx_call_args }}),
        args.rval());
    {% else %}
    api_{{ func.fct_name }}({{ func.fct_arg|cxx_call_args }});
    args.rval().setUndefined();
    {% endif %}
    return true;
}

{% endfor %}

static JSFunctionSpec api_functions[] = {
    {% for func in game.function %}
    JS_FN("{{ func.fct_name|camel_case(upper=False) }}", 
          js_{{ func.fct_name }}, {{ func.fct_arg|length }}, 0),
    {% endfor %}
    JS_FS_END
};

{% for enum in game.enum %}
bool define_enum_{{ enum.enum_name }}(JSContext* cx, JS::HandleObject global) {
    JS::RootedObject obj(cx, JS_NewPlainObject(cx));
    JS::RootedValue i(cx);
    {% for field_name, _ in enum.enum_field %}
    i = JS::Int32Value({{ loop.index0 }});
    JS_SetProperty(cx, obj, "{{ field_name|camel_case }}", i);
    {% endfor %}

    JS_FreezeObject(cx, obj);
    JS::RootedValue val(cx, JS::ObjectValue(*obj));
    JS_SetProperty(cx, global, "{{ enum.enum_name|camel_case }}", val);
    return true;
}

{% endfor %}
{% for constant in game.constant %}
void define_const_{{ constant.cst_name|lower }}(JSContext* cx,
                                                JS::HandleObject global) {
    JS::RootedValue val(cx, {{ constant|js_const_val }});
    JS_SetProperty(cx, global, "{{ constant.cst_name }}", val);
}

{% endfor %}

bool js_console_log_inner(JSContext* cx, unsigned argc, JS::Value* vp, 
                          std::ostream& out) {
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    for (unsigned i = 0; i < argc; i++) {
        JS::RootedString js_str(cx, JS::ToString(cx, args[i]));
        if (!js_str) {
            JS_ReportErrorASCII(cx, "couldn't convert value to string");
            return false;
        }
        std::string str;
        if (!js_encode_string(cx, js_str, str))
            out << "??? ";
        else
            out << str << " ";
    }
    out << std::endl;
    return true;
}

bool js_console_log(JSContext* cx, unsigned argc, JS::Value* vp) {
    return js_console_log_inner(cx, argc, vp, std::cout);
}

bool js_console_error(JSContext* cx, unsigned argc, JS::Value* vp) {
    return js_console_log_inner(cx, argc, vp, std::cerr);
}

void define_js_console(JSContext* cx, JS::HandleObject global) {
    JS::RootedObject console(cx, JS_NewPlainObject(cx));
    JS_DefineFunction(cx, console, "log", js_console_log, 0, 0);
    JS_DefineFunction(cx, console, "error", js_console_error, 0, 0);
    JS::RootedValue console_val(cx, JS::ObjectValue(*console));
    JS_SetProperty(cx, global, "console", console_val);
}

static bool initialized = false;

// Call `JS_ShutDown()` to avoid a segfault on exit
class AutoCleanup {
public:
    ~AutoCleanup() {
        if (initialized)
            JS_ShutDown();
    }
};

static AutoCleanup _cleanup;
static JSContext* cx;
static JS::PersistentRootedObject global;

static void init_js() {
    if (initialized)
        return;
    initialized = true;

    if (!JS_Init())
        throw 42;
    cx = JS_NewContext(JS::DefaultHeapMaxBytes);
    if (!JS::InitSelfHostedCode(cx))
        throw 42;

    static JSClass GlobalClass = {"Stechec", JSCLASS_GLOBAL_FLAGS, &JS::DefaultGlobalClassOps};
    global = JS::PersistentRootedObject(
        cx, JS_NewGlobalObject(cx, &GlobalClass, nullptr,
                               JS::FireOnNewGlobalHook, JS::RealmOptions()));
    JS::EnterRealm(cx, global);

    define_js_console(cx, global);
    JS_DefineFunctions(cx, global, api_functions);
    {% for enum in game.enum %}
    define_enum_{{ enum.enum_name }}(cx, global);
    {% endfor %}
    {% for constant in game.constant %}
    define_const_{{ constant.cst_name|lower }}(cx, global);
    {% endfor %}
    if (JS_IsExceptionPending(cx))
        throw 42;

    const char* champion_path = getenv("CHAMPION_PATH");
    std::string champion;

    if (!champion_path)
        champion = "champion.js";
    else {
        champion = champion_path;
        champion += "/champion.js";
    }
    JS::RootedValue _rval(cx);
    if (!JS::EvaluateUtf8Path(cx, JS::OwningCompileOptions(cx), 
                              champion.c_str(), &_rval)) {
        throw 42;
    }
}

static void js_report_error() {
    if (!JS_IsExceptionPending(cx))
        return;
    JS::RootedValue exc(cx);
    JS::ExceptionStack stack(cx);
    JS_GetPendingException(cx, &exc);
    JS::GetPendingExceptionStack(cx, &stack);
    JS_ClearPendingException(cx);

    JS::ErrorReportBuilder report(cx);
    report.init(cx, stack, JS::ErrorReportBuilder::WithSideEffects);
    JS::PrintError(stderr, report, true);

    JS::RootedString js_bt(cx);
    if (!stack.stack() || 
        !JS::BuildStackString(cx, nullptr, stack.stack(), &js_bt, 2)) {
        std::cerr << "(no stack trace available)" << std::endl;
        return;
    }
    std::string bt;
    js_encode_string(cx, js_bt, bt);
    std::cerr << bt;
}

{% for func in game.user_function %}
extern "C" {{ func|cxx_prototype }} {
    init_js();

    JS::RootedValue js_ret(cx);
    {% if func.fct_arg %}
    JS::RootedValueArray<{{ func.fct_arg|length }}> args(cx);
    {% for arg_name, arg_type, _ in func.fct_arg %}
    {{ arg_type|cxx_to_js }}(cx, {{ arg_name }}, args[{{ loop.index0 }}]);
    {% endfor %}
    if (JS_IsExceptionPending(cx))
        throw 42;
    {% else %}
    JS::HandleValueArray args = JS::HandleValueArray::empty();
    {% endif %}

    if (!JS_CallFunctionName(cx, global, 
        "{{ func.fct_name|camel_case(upper=False) }}", args, &js_ret)) {
        js_report_error();
        mozalloc_abort("");
    }
    {% if func is returning %}

    {{ func.fct_ret_type|cxx_type }} cxx_ret;
    if (!{{ func.fct_ret_type|js_to_cxx }}(cx, js_ret, cxx_ret)) {
        std::cerr
            << "`{{ func.fct_name|camel_case(upper=False) }}` returned bad "
               "value, expected type `{{ func.fct_ret_type|ts_type }}`"
            << std::endl;
        mozalloc_abort("");
    }
    return cxx_ret;
    {% endif %}
}

{% endfor %}
